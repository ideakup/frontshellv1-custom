<?php

/* atiken */
return [
    'staticSlugs' => [
        'tags' => 'tags',
        'categories' => 'categories'
    ],
    'siteDefaults' => [
        'fontFamily' => 'family=Playfair+Display:wght@500&family=Poppins&display=swap',
        'topBar' => true,// false, true 
        'footerType' => 1, //0->iletişim, son makale, menü, 1->linkler, abone ol, iletişim, 2->frontshell, iletişim, menü, 3->frontshell, iletişim 4-->servo 5-->kaymakci
        'logoExtension' => 'png',//png, svg
    ],
    'pageProperties' => [

        'default' => [
            'bodyClass' => '',
            'topbarClass' =>'dark',
            'headerClass' => '',
            'contentBgcolor' => '#FFFFFF',
            'containerClass' => 'container',    
            'sliderClass' => '',
            'sliderNavVisible' => true,
            'pageTitle' => false,
        ]

    ],
    'stafftype'=>[
        'kurucu_avukat' => [
            'name' => 'Kurucu Avukat',
            'value' => 'kurucu_avukat'
        ],
        'avukat' => [
            'name' => 'Avukat',
            'value' => 'avukat'
        ],
        'personel' => [
            'name' => 'Personel',
            'value' => 'personel'
        ]


    ],
    
];
